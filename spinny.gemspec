# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'spinny/version'

Gem::Specification.new do |spec|
  spec.name          = "spinny"
  spec.version       = Spinny::VERSION
  spec.authors       = ["Marie Markwell"]
  spec.email         = ["me@marie.so"]
  spec.summary       = %q{Reinventing Ruby, because why not.}
  spec.description   = spec.summary
  spec.homepage      = "https://gitlab.com/spinny/spinny"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  # Utilities.
  spec.add_runtime_dependency 'spinny-utilities', '~> 0.2.0'

  # Variable manipulation
  spec.add_runtime_dependency 'lovama', '~> 0.2.3'
  spec.add_runtime_dependency 'spinny-global_variables', '~> 0.0.2'

  # Improvements of existing functionality.
  spec.add_runtime_dependency 'net-socket', '~> 1.0.0'
  spec.add_runtime_dependency 'spinny-namespace', '~> 0.1.0'

  # Alternative approaches to existing idioms.
  spec.add_runtime_dependency 'from', '~> 0.1.0'
  spec.add_runtime_dependency 'default', '~> 0.2.0'

  # Possibly a bit too close to actually modifying Ruby's syntax.
  #spec.add_runtime_dependency 'letty', '~> 0.0.2'
  #spec.add_runtime_dependency 'spinny-quick_lambda', '~> 0.0.1'
  #spec.add_runtime_dependency 'tinyregexp'
  #spec.add_runtime_dependency 'woodpecker'
  #spec.add_runtime_dependency 'dehash'

  # Non-Spinny libraries.
  spec.add_runtime_dependency 'contracts', '~> 0.4'
  spec.add_runtime_dependency 'mayhaps', '~> 0.3.0'

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
end
