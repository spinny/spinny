class String
  # "{foo} {1} {0}".format('bar', 'baz', foo: 'foo') #=> "foo baz bar"
  def format(*args)
    # Using this instead of **hsh for 1.9.3 support.
    hsh = args.last.is_a?(Hash) ? args.pop : {}

    self.clone.gsub(/{(\S+)}/) do
      hsh[$1] || hsh[$1.to_sym] || args[$1.to_i]
    end
  end
end
