require 'spinny/version'

module Spinny
  require 'spinny/utilities'

  require 'lovama'
  require 'spinny/global_variables'

  require 'net/socket'
  require 'spinny/namespace'

  require 'from'
  require 'default'

  #require 'letty'
  #require 'spinny-quick_lambda'
  #require 'tinyregexp'
  #require 'woodpecker'
  #require 'dehash'

  # Non-Spinny libraries.
  require 'contracts'
  require 'mayhaps'
end
