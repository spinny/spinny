require 'spec_helper'


describe 'String#format' do
  it 'has numbered references' do
    "foo {1} {0}".format('bar', 'baz')
  end

  it 'has named indexing' do
    "foo {c} {b} {0}".format('a', b: 'b', c: 'd')
  end
end

