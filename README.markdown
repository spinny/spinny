# Spinny

Reinventing Ruby for my own amusement.

## Installation

Add this line to your application's Gemfile:

    gem 'spinny'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install spinny

## Usage

### String#format()

```ruby
require 'spinny'

"{foo} {1} {0}".format('baz', 'bar', foo: 'foo') #=> "foo bar baz"
```

### Nicer default values for methods

Courtesy of the [default](https://gitlab.com/spinny/default) gem.

```ruby
require 'spinny'

def test(arg1 = default)
  # This is just syntactic sugar for `arg1 == default`
  arg1.default?
end

test               #=> true
test(1)            #=> false
test(nil)          #=> false
test(:default)     #=> false # Common alternative to nil.
test(DefaultValue) #=> true  # Well, it has to be equal to something, now doesn't it?


def test2(arg1 = default)
  arg1
end

test2 #=> DefaultValue


def test3(arg1 = default)
  default_for(:arg1) { "default value for arg1!" }

  arg1
end

test3         #=> "default value for arg1"
test3("foo")  #=> "foo"
```

### Contracts

Courtesy of the [Contracts](http://egonschiele.github.io/contracts.ruby/) gem.

Way too much to put here -- read that page!

`require 'spinny'` will require Contracts, as well.

### Function assignment shorthand

Courtesy of the [letty](https://gitlab.com/spinny/letty) gem.

```ruby
require 'spinny'

class Loggy
  fn log´ = ->(cond, io, text) { io.puts(text) if cond }
  fn log  = :log´.(true, $stdout)
end

Loggy.new.log('hello')
```

### Local variable manipulation

Courtesy of the [lovama](https://gitlab.com/spinny/lovama) gem. Be sure to take note of its [limitations](https://gitlab.com/spinny/lovama/blob/main/README.markdown#limitations).

```ruby
require 'spinny'

foo = true

foo #=> true
local_variable_get(:foo) #=> true

local_variable_set(:foo, false) #=> false

foo #=> false
local_variable_get(:foo) #=> false
```

### An alternative global variable implementation

Courtesy of the [spinny-global_variables](https://gitlab.com/spinny/spinny-global_variables) gem.

Reimplements globals entirely, providing `global_variable_set`, `global_variable_get`, and `global_variable_defined`.

Since you lose the `$X = ...` syntax, it provides a `global` helper method.

```ruby
require 'spinny'

global_variable_get(:foo) #=> nil
global_variable_defined?(:foo) #=> false

global_variable_set(:foo, 'bar') #=> 'bar'

global_variable_get(:foo) #=> 'bar'
global_variable_defined?(:foo) #=> true

global :foo #=> 'bar'
global foo: 'baz' #=> {foo: 'baz'}
global :foo #=> 'baz'
global_variable_get(:foo) #=> 'baz'
```

### Mayhaps

[@programble](https://github.com/programble)'s [mayhaps](https://github.com/programble/mayhaps) gem allows you to call methods on objects that may or may not be nil.

```ruby
require 'spinny'

hash = {:foo => " foo "}

hash[:foo].maybe.upcase #=> #<Just " FOO ">
hash[:bar].maybe.upcase #=> #<Nothing>

+hash[:foo].maybe.upcase #=> " FOO "
+hash[:bar].maybe.upcase #=> nil

+hash[:foo].maybe.upcase.strip #=> "FOO"
+hash[:bar].maybe.upcase.strip #=> nil
```

### Quick Lambdas

Courtesy of the [spinny-quick_lambda](https://gitlab.com/spinny/spinny-quick_lambda) gem.

Adds a nicer lambda syntax, similar to what Scala offers. (Name courtesy of [macropy](https://github.com/lihaoyi/macropy#quick-lambdas).)

```ruby
require 'spinny'

[1, 2, 3, 4].map(& _ + 2 + 3 ) #=> [6, 7, 8, 9]
```

## Future additions

* [a tiny Regular Expression implementation](https://gitlab.com/spinny/tinyregexp)
* [hash deconstruction](https://gitlab.com/spinny/dehash)
* function composition features

## Contributing

1. Fork it ( https://gitlab.com/spinny/spinny/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request
